package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	private static final long serialVersionUID = 7802515668491130811L;
	
	private ArrayList<String> data = new ArrayList<>();
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
		}
	 
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		System.getProperties().put("cdetails", "Joshua, Orbiso, joshgorbiso372@gmail.com, 09069263549");
		String cdetails = System.getProperty("details");
		PrintWriter out = res.getWriter();
		out.println(cdetails);
		
		HttpSession session = req.getSession();
		session.setAttribute("lname", "Giganto");
		
		res.sendRedirect("user?cdetails="+cdetails);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		ServletContext srvContext = getServletContext();
		
		String roomName = req.getParameter("roomtype");
		data.add(roomName);
		
		srvContext.setAttribute("data", data);
		
		RequestDispatcher rd = req.getRequestDispatcher("user");
		rd.forward(req, res);
		
		PrintWriter out = res.getWriter();
		out.println(data);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroyed. ");
		System.out.println("******************************************");
		}
	
}
