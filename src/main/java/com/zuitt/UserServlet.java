package com.zuitt;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ArrayList<String> data = new ArrayList<>();
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
		}	

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
		data.add(fname);
		data.add(lname);
		data.add(email);
		data.add(contact);
		
		PrintWriter out = res.getWriter();
		out.println(data);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
	ServletContext srvContext = getServletContext();
	String fname = srvContext.getInitParameter("first_name");
	String lname = srvContext.getInitParameter("last_name");
	String email = srvContext.getInitParameter("email");
	String contact = srvContext.getInitParameter("contact");
	
	PrintWriter output = res.getWriter();
	output.println(
			"<h1>Welcome to Phonebook</h1>" +
			"<p>First Name: " + fname + "</p>" +
			"<p>Last Name: " + lname + "</p>" +
			"<p>Email: " + email + "</p>" +
			"<p>Contact: " + contact + "</p>"
 			);
	}	
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("******************************************");
		}	
	
}
